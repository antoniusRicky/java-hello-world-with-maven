## Review Checklist

### Review Business Flow

| No  | Item                                                                                                     | Status                                                                                                                                                                      |
|-----|----------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | User Flow                                                                                                | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 2   | Functional Flow                                                                                          | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 3   | Error Handling (APX)                                                                                     | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 4   | Bilingual                                                                                                | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 5   | Partial Update                                                                                           | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 6   | Partial Maintenance                                                                                      | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 7   | Financial Activation                                                                                     | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 8   | Related impact to existing feature                                                                       | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 9   | Documentation created / updated : [check folder](http://bcagitlab/OmniChannel/Documentation/-/tree/DEV1) | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |
| 10  | Do the views and styles follow the company’s design guidelines?                                          | &nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp; |



### Review Technical

[Other reference checklist](https://bcaoffice365.sharepoint.com/:x:/r/sites/MobileDevelopmentTeam-OmniMobile/Shared%20Documents/Omni%20Mobile/Guideline/Review%20Checklist.xlsx?d=w2ee1c1575a2a4d3c86fd14925a02023d&csf=1&web=1&e=WyKOKO)

<style type="text/css">
.tg  {border-collapse:collapse;border-color:#ccc;border-spacing:0;}
.tg td{background-color:#fff;border-color:#ccc;border-style:solid;border-width:1px;color:#333;
  font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{background-color:#f0f0f0;border-color:#ccc;border-style:solid;border-width:1px;color:#333;
  font-family:Arial, sans-serif;font-size:14px;font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-c3ow"><b>No</b></th>
    <th class="tg-c3ow"><b>Item</b></th>
    <th class="tg-c3ow"><b>Status</b></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky" colspan="3"><b>Implementation</b></td>
  </tr>
  <tr>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Does the code follow the project’s architectural patterns? <a href="http://bcagitlab/OmniChannel/FrontEnd-Mobile/Omni-Android/-/wikis/Understanding-the-Architecture">see guide</a> <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> presentation, domain dan data sudah sesuai ketentuan atau belum. <br/> ex : data tidak boleh panggil presentation. <br/> cek by import list. <br/> a. dependency rules <br/> b. penulisan komponen masing-masing layer <br/> ex : <br/> -presentation (view & mapper ke domain layer) <br/> -domain (penulisan yg berhub. dgn use case, entity object sama repository interfacenya) <br/> -data (realm, retrofit, repository implementasi) <br/> </div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Does the code follow the project’s coding style guide? <a href="http://bcagitlab/OmniChannel/FrontEnd-Mobile/Omni-Android/-/wikis/myBCA-Code-Guideline">see guide</a></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">Is there any code that becomes obsolete after the code changes and therefore can be deleted?</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky" colspan="3"><b>Error handling and logging</b></td>
  </tr>
  <tr>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Are remote non-fatal errors being logged?  <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> Conditional. kalau record wajib pakai catcher, tidak boleh langsung pakai firebase</div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">What happens when an API call fails? check showErrorMessageV3</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">What happens when the device is rotated? set portrait in manifest</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">What happens if some permissions that the feature requires are denied? check on error message. Permission required?</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky">Handling common error code & spesific feature <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> Common handling error code utk transaksi fin</div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">Spesific OS handling <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide">Discuss di pre-dev utk variasi OS yang perlu dicek. Sewaktu review memastikan hal tsb sudah di handle atau belum.</div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky" colspan="3"><b>Usability and Accessibility</b></td>
  </tr>
  <tr>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">What happens if a button with a specific action was clicked twice or thrice? Does it have the correct handling ?</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Are the correct view classes being used? -> component. New component put to omni design. Existing -> reused <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> make sure developer reuse existing component if necessary. ex : loading, error state, pop up </div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">Are you using font & colors from the company’s branding? <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> only check for changes in color area. either by hex or by naming, make sure changes equals with iOS </div> </td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">How does the feature look with non-default font and display sizes? -> fix size dp utk font, margin, guideline.</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky">Text masuk ke strings.xml (dua bahasa). default value (jika ada). input type di edit text sudah sesuai? (pre dev & review)</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">View Binding <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> sudah menggunakan view binding, kecuali view holder. View Holder pakai findviewbyID </div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">7</td>
    <td class="tg-0pky">Dont keep activities  <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> mention di predev, perlu skenario SIT. </br></br> make sure sudah handling case dont keep activities, tidak crash, value sebelumnya ada. </br></br> Setting pilih dont keep activities, buka layar, put ke background, back to app. Untuk skenario positif only </div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">8</td>
    <td class="tg-0pky">Unsubcribe use case di presenter (case MVP)</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">9</td>
    <td class="tg-0pky">Use animation in fragment transition
<img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide">
            app:enterAnim="@anim/left_in" 
            app:exitAnim="@anim/left_out" 
            app:popEnterAnim="@anim/right_in" 
            app:popExitAnim="@anim/right_out" </div></td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky" colspan="3"><b>Readability</b></td>
  </tr>
  <tr>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Are there any comments that are no longer accurate after the code changes and should be updated? Remove unused comment.</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky" colspan="3"><b>Dependencies</b></td>
  </tr>
  <tr>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">Does Google recommend using the library or an alternative?</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">Library used?</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">If there’s a version bump, does the new version introduce any breaking changes?</td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>
  <tr>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">Dexguard, does an obfuscated build of the app still work as expected or do the obfuscation rules need to be updated?  <img class="myDIV" src=".assets/info_icon.jpg" height="15px;" width="15px"/><div class="hide"> check by doc SIT <br>
- apakah di fitur baru ini, perlu ditambahkan rules khusus di dexguard? misal ada penambahan lib baru vidcall <br>
- apakah dgn dexguard naik versi, code mybca perlu ada berubah? misal method init atau lainnya. </div> </td>
    <td class="tg-0pky">&nbsp; &nbsp; <input type="radio" name="status"/> Y &nbsp; &nbsp; <input type="radio" name="status"/> N  &nbsp; &nbsp;<input type="radio" name="status"/> N/A &nbsp; &nbsp;</td>
  </tr>

</tbody>
</table>

<style>
.hide {
display: none;
}

.myDIV:hover + .hide {
display: block;
color: #006599;
}
</style>
